﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTest.Core.Models
{
    public class Courier
    {
        public int Id { get; set; }
        public string Name { get;  set; }
        public string Category { get; set; }
        public bool IsActive { get; set; }
    }
}
