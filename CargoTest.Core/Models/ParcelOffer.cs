﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTest.Core.Models
{
    public class ParcelOffer
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public decimal Weight { get; set; }
        public decimal Volume { get; set; }
        public decimal FinalPrice { get; set; }
        public Courier Courier { get; set; }
    }
}
