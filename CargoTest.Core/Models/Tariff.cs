﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTest.Core.Models
{
    public class Tariff
    {
        public int Id { get; set; }
        public short TariffTypeId { get; set; } //1:Weight, 2: Volume
        public int CourierId { get; set; }
        public decimal? MinValue { get; set; }
        public decimal? MaxValue { get; set; }
        public decimal Fee { get; set; }
        public bool IsOverweightPerFee { get; set; }
        public decimal OverweightPerFee { get; set; }
        public bool IsActive { get; set; }
    }
}
