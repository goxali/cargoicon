﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTest.Core.Models
{
    public class Order
    {
        public long Id { get; set; }
        public long ParcelOfferId { get; set; }
        public short Status { get; set; }
        public bool IsActive { get; set; }
    }
}
