﻿using CargoTest.Core.Models;
using Microsoft.EntityFrameworkCore.InMemory.ValueGeneration.Internal;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.Metrics;
using System.Reflection.Metadata;

namespace CargoTest.Core.Repository
{
    public class CargoRepository : ICargoRepository
    {
        public async Task<int> AddCourierAsync(Courier courier)
        {
            using (var context = new CargoDbContext())
            {
                courier.Id = context.Couriers.Max(x => x.Id) + 1;
                context.Couriers.Add(courier);
                var id = await context.SaveChangesAsync();
                return courier.Id;
            }
        }

        public async Task<long> AddOrderAsync(Order order)
        {
            using (var context = new CargoDbContext())
            {
                order.Id = (context.Orders == null || !context.Orders.Any()) ? 1 : context.Orders.Max(x => x.Id) + 1;
                context.Orders.Add(order);
                await context.SaveChangesAsync();
                return order.Id;
            }
        }

        public async Task<long> AddParcelOfferAsync(ParcelOffer parcelOffer)
        {
            using (var context = new CargoDbContext())
            {
                context.Entry(parcelOffer).State = EntityState.Modified;
                parcelOffer.Id = (!context.ParcelOffers.Any()) ? 1 : context.ParcelOffers.Max(x => x.Id) + 1;
                context.ParcelOffers.Add(parcelOffer);
                await context.SaveChangesAsync();
                return parcelOffer.Id;
            }
        }

        public Courier GetCourier(int id)
        {
            using (var context = new CargoDbContext())
            {
                var courier = context.Couriers.FirstOrDefault(x => x.IsActive && x.Id == id);
                return courier;
            }
        }

        public async Task<List<Courier>> GetCouriers()
        {
            using (var context = new CargoDbContext())
            {
                var courierList = context.Couriers.Where(x => x.IsActive)
                    .ToListAsync();
                return await courierList;
            }
        }

        public Order GetOrder(long id)
        {
            using (var context = new CargoDbContext())
            {
                var order = context.Orders.FirstOrDefault(x => x.IsActive && x.Id == id);
                return order;
            }
        }

        public List<Order> GetOrders()
        {
            using (var context = new CargoDbContext())
            {
                var orderList = context.Orders.Where(x => x.IsActive).ToList();
                return orderList;
            }
        }

        public List<Order> GetOrdersByUserId(int userId)
        {
            using (var context = new CargoDbContext())
            {
                var orderList = context.Orders.Where(x => x.IsActive).ToList();
                return orderList;
            }
        }

        public async Task<ParcelOffer> GetParcelOffer(long userId)
        {
            using (var context = new CargoDbContext())
            {
                var parcelOfferList = await context.ParcelOffers.Include(x => x.Courier).Where(x => x.UserId == userId).ToListAsync();
                return parcelOfferList.FirstOrDefault();
            }
        }

        public List<ParcelOffer> GetParcelOffers(long userId)
        {
            using (var context = new CargoDbContext())
            {
                var parcelOfferList = context.ParcelOffers.Include(x => x.Courier).Where(x => x.UserId == userId).ToList();
                return parcelOfferList;
            }
        }

        public List<Tariff> GetTariffs()
        {
            using (var context = new CargoDbContext())
            {
                var tariffList = context.Tariffs.Where(x => x.IsActive).ToList();
                return tariffList;
            }
        }

        public async Task<List<Tariff>> GetTariffsByCourierIdAsync(int courierId)
        {
            using (var context = new CargoDbContext())
            {
                var tariffList = context.Tariffs.Where(x => x.IsActive && x.CourierId == courierId).ToListAsync();
                return await tariffList;
            }
        }

        public List<TariffType> GetTariffTypes()
        {
            var tarifftypeList = Enum.GetValues(typeof(TariffType)).Cast<TariffType>().ToList();
            return tarifftypeList;

        }

        public async Task<User?> GetUserById(long id)
        {
            using var context = new CargoDbContext();
            var user = context.Users.FirstOrDefaultAsync(x => x.Id == id);
            return await user;
        }

        public async Task<User?> GetUserByLogin(string userName, string password)
        {
            using var context = new CargoDbContext();
            var user = context.Users.FirstOrDefaultAsync(x => x.UserName == userName && x.Password == password);
            return await user;
        }

        public async Task UpdateCourierAsync(Courier courier)
        {
            using (var context = new CargoDbContext())
            {
                var entity = context.Couriers.FirstOrDefault(item => item.Id == courier.Id);
                if (entity != null)
                {
                    entity.Name = courier.Name;
                    entity.Category = courier.Category;
                    entity.IsActive = courier.IsActive;
                    await context.SaveChangesAsync();
                }
            }
        }
    }
}
