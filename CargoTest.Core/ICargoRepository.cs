﻿using CargoTest.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTest.Core
{
    public interface ICargoRepository
    {
        public Task<List<Courier>> GetCouriers();
        public Courier GetCourier(int id);
        public Task<List<Tariff>> GetTariffsByCourierIdAsync(int courierId);
        public List<Tariff> GetTariffs();
        public List<TariffType> GetTariffTypes();
        public List<ParcelOffer> GetParcelOffers(long userId);
        public List<Order> GetOrders();
        public Order GetOrder(long id);
        public Task<int> AddCourierAsync(Courier courier);
        public Task<long> AddParcelOfferAsync(ParcelOffer parcelOffer);
        public Task<ParcelOffer> GetParcelOffer(long userId);
        public Task UpdateCourierAsync(Courier courier);
        public Task<long> AddOrderAsync(Order order);
        public List<Order> GetOrdersByUserId(int userId);
        public Task<User?> GetUserByLogin(string userName, string password);
        public Task<User?> GetUserById(long id);
    }
}
