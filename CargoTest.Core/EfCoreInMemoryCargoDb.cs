﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargoTest.Core.Models;
using System.Reflection.Metadata;
using Microsoft.EntityFrameworkCore.InMemory.ValueGeneration.Internal;

namespace CargoTest.Core
{
    public class CargoDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "CargoDb");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
        public DbSet<Courier> Couriers { get; set; }
        public DbSet<Tariff> Tariffs { get; set; }
        public DbSet<ParcelOffer> ParcelOffers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<User> Users { get; set; }
    }

}
