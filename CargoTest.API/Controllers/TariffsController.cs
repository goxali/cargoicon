﻿using CargoTest.Core;
using CargoTest.Core.Models;
using CargoTest.Core.Repository;
using CargoTest.Core.Workflow;
using CargoTest.Workflow.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CargoTest.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class TariffsController : ControllerBase
    {
        readonly ICargoRepository _cargoRepository;
        readonly TariffManager _tariffManager;
        private readonly ILogger<ParcelOffersController> _logger;
        public TariffsController(ILogger<ParcelOffersController> logger, ICargoRepository cargoRepository)
        {
            _logger = logger;
            _cargoRepository = cargoRepository;
            _tariffManager = new TariffManager(_cargoRepository);
        }

        // GET: api/<TariffController>
        [HttpGet]
        public async Task<ActionResult<GetTariffsResponse>> Get() => Ok(await _tariffManager.GetTariffsAsync());

        // GET api/<TariffController>/5
        [HttpGet("{courierId}")]
        public async Task<ActionResult<GetTariffsResponse>> Get(int courierId) => Ok(await _tariffManager.GetTariffsByCourierIdAsync(courierId));


        // GET: api/<TariffController>
        [HttpGet("[action]")]
        public async Task<ActionResult<GetTariffTypesResponse>> GetTarifftypes() => Ok(await _tariffManager.GetTariffTypesAsync());
    }
}
