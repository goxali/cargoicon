﻿using CargoTest.Core;
using CargoTest.Workflow.Model;
using CargoTest.Core.Workflow;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace CargoTest.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class ParcelOffersController : ControllerBase
    {

        readonly ICargoRepository _cargoRepository;
        readonly ParcelOfferManager _parcelOfferManager;
        private readonly ILogger<ParcelOffersController> _logger;
        public ParcelOffersController(ILogger<ParcelOffersController> logger,ICargoRepository cargoRepository)
        {
            _logger = logger;
            _cargoRepository = cargoRepository;
            _parcelOfferManager = new ParcelOfferManager(_cargoRepository);

        }
        // GET: api/<ParcelOffersController>
        [HttpGet]
        public async Task<ActionResult<GetParcelOfferResponse>> Get([FromQuery] GetParcelOfferRequest parcelOfferRequest) => Ok(await _parcelOfferManager.GetParcelOffersAsync(parcelOfferRequest));

        // GET api/<ParcelOffersController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GetParcelOfferByUserIdResponse>> Get(long userId)
        {
            var getParcelOfferByIdRequest = new GetParcelOfferByUserIdRequest();
            getParcelOfferByIdRequest.UserId = userId;
            return Ok(await _parcelOfferManager.GetParcelOfferByUserIdAsync(getParcelOfferByIdRequest));
        }

        // POST api/<ParcelOffersController>
        [HttpPost]
        public async Task<ActionResult<AddParcelOfferResponse>> Post([FromBody] AddParcelOfferRequest addParcelOfferRequest) => Ok(await _parcelOfferManager.AddParcelOfferAsync(addParcelOfferRequest));

    }
}
