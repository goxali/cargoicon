﻿using CargoTest.Core;
using CargoTest.Core.Models;
using CargoTest.Core.Workflow;
using CargoTest.Workflow.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CargoTest.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class OrdersController : ControllerBase
    {
        readonly ICargoRepository _cargoRepository;
        readonly OrderManager _orderManager;
        private readonly ILogger<ParcelOffersController> _logger;
        public OrdersController(ILogger<ParcelOffersController> logger, ICargoRepository cargoRepository)
        {
            _logger = logger;
            _cargoRepository = cargoRepository;
            _orderManager= new OrderManager(cargoRepository);
        }

        // GET: api/<OrderController>
        [HttpGet]
        public async Task<ActionResult<GetOrderResponse>> Get() => Ok(await _orderManager.GetOrdersAsync());

        // GET api/<OrderController>/5
        [HttpGet("{userId}")]
        public async Task<ActionResult<GetOrderResponse>> Get(int userId) => Ok(await _orderManager.GetOrdersByUserIdAsync(userId));

        // POST api/<OrderController>
        [HttpPost]
        public async Task<ActionResult<AddOrderResponse>> Post([FromBody] AddOrderRequest order) => Ok(await _orderManager.AddOrderAsync(order));

    }
}
