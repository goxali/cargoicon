﻿using CargoTest.Core;
using CargoTest.Core.Models;
using CargoTest.Core.Workflow;
using CargoTest.Workflow.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using System.Runtime.CompilerServices;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CargoTest.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController, Authorize]
    public class CouriersController : ControllerBase
    {
        readonly ICargoRepository _cargoRepository;
        readonly CourierManager _courierManager;
        private readonly ILogger<ParcelOffersController> _logger;
        public CouriersController(ILogger<ParcelOffersController> logger, ICargoRepository cargoRepository)
        {
            _logger = logger;
            _cargoRepository = cargoRepository;
            _courierManager = new CourierManager(_cargoRepository);
        }

        // GET: api/<CouriersController>
        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json)]
        //[ProducesResponseType(StatusCodes.Status201Created)]
        //[ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<GetCourierResponse>> Get() => Ok(await _courierManager.GetCouriersAsync());

        // GET api/<CouriersController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GetCourierByIdResponse>> Get(int id) => Ok(await _courierManager.GetCourierByIdAsync(id));

        // POST api/<CouriersController>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] AddCourierRequest courier) => Ok(await _courierManager.AddCourierAsync(courier));

        // PUT api/<CouriersController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] UpdateCourierRequest courier) => Ok(await _courierManager.UpdateCourierAsync(courier));

        // DELETE api/<CouriersController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var courier = await _courierManager.GetCourierByIdAsync(id);
            courier.CourierItem.IsActive = false;
            var updateCourierRequest = new UpdateCourierRequest();
            updateCourierRequest.CourierItem = courier.CourierItem;
            return Ok(await _courierManager.UpdateCourierAsync(updateCourierRequest));
        }
    }
}
