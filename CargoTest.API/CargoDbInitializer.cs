﻿using CargoTest.Core;
using CargoTest.Core.Models;

namespace CargoTest.API
{
    public class CargoDbInitializer
    {
        public static void Seed(WebApplication app)
        {

            using var scope = app.Services.CreateScope();
            var services = scope.ServiceProvider;
            var cargoDbContext = services.GetService<CargoDbContext>();


            cargoDbContext.Users.AddRange(
                new User() { Id = 1, UserName = "ali", Name = "Ali", SurName = "Goksal", Email="a@a.com",FullName="Ali Goksal", Password="ali" }
                );

            cargoDbContext.Couriers.AddRange(
                new Courier() { Id = 1, Name = "Cargo4You", Category = "Internal", IsActive = true },
                new Courier() { Id = 2, Name = "ShipFaster", Category = "External", IsActive = true },
                new Courier() { Id = 3, Name = "MaltaShip", Category = "External", IsActive = true }
                );

            var tariffs = new List<Tariff>
                {
                    new Tariff() { Id= 1, CourierId = 1, TariffTypeId = 1, MinValue = 0, MaxValue = 2, Fee = 15, IsActive=true},
                    new Tariff() { Id= 2, CourierId = 1, TariffTypeId = 1, MinValue = 2, MaxValue = 15, Fee = 18, IsActive = true},
                    new Tariff() { Id= 3, CourierId = 1, TariffTypeId = 1, MinValue = 15, MaxValue = 20, Fee = 35, IsActive = true},
                    new Tariff() { Id= 4, CourierId = 2, TariffTypeId = 1, MinValue = 10, MaxValue = 15, Fee = 9.50M,IsActive=true},
                    new Tariff() { Id= 5, CourierId = 2, TariffTypeId = 1, MinValue = 15, MaxValue = 25, Fee = 36.50M, IsActive = true},
                    new Tariff() { Id= 6, CourierId = 2, TariffTypeId = 1, MinValue = 25, MaxValue = 30, Fee = 40, IsOverweightPerFee = true, OverweightPerFee = 0.417M, IsActive= true},
                    new Tariff() { Id= 7, CourierId = 3, TariffTypeId = 1, MinValue = 10, MaxValue = 20, Fee = 16.99M, IsActive = true},
                    new Tariff() { Id= 8, CourierId = 3, TariffTypeId = 1, MinValue = 20, MaxValue = 30, Fee = 33.99M, IsActive = true},
                    new Tariff() { Id= 9, CourierId = 3, TariffTypeId = 1, MinValue = 30, MaxValue = null, Fee = 43.99M, IsOverweightPerFee = true, OverweightPerFee = 0.41M  , IsActive=true},
                    new Tariff() { Id= 10, CourierId = 1, TariffTypeId = 2, MinValue=0, MaxValue=1000, Fee=10,IsActive=true},
                    new Tariff() { Id= 11, CourierId = 1, TariffTypeId = 2, MinValue=1000, MaxValue=2000, Fee=20, IsActive = true},
                    new Tariff() { Id= 12, CourierId = 2, TariffTypeId = 2, MinValue=0, MaxValue=1000, Fee=11.99M, IsActive = true},
                    new Tariff() { Id= 13, CourierId = 2, TariffTypeId = 2, MinValue = 1000, MaxValue = 1700, Fee=21.99M, IsActive = true},
                    new Tariff() { Id= 14, CourierId = 3, TariffTypeId = 2, MinValue = 500, MaxValue = 1000, Fee=9.50M, IsActive = true},
                    new Tariff() { Id= 15, CourierId = 3, TariffTypeId = 2, MinValue = 1000, MaxValue = 2000, Fee=19.50M, IsActive = true},
                    new Tariff() { Id= 16, CourierId = 3, TariffTypeId = 2, MinValue=2000, MaxValue=5000, Fee=48.50M, IsActive = true},
                    new Tariff() { Id= 17, CourierId = 3, TariffTypeId = 2, MinValue=5000, MaxValue=null, Fee=147.50M, IsActive = true}
                };
            cargoDbContext.Tariffs.AddRange(tariffs);
            cargoDbContext.SaveChanges();

        }
    }
}
