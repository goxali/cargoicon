
How to run app
---------------------------
--Backend

Open CargoTest.sln in Vs studio

Run API project 

--Frontend

Open CargoTest.UI folder in vs code

run npm start in root folder 

see the master page 

login user :ali

password :ali

Tech stack
-----------------------------
--Backend

Asp DotNet Core 6

EF Core 7

EF Core In Memory DB 7


--Frontend

React.js 18

React-Bootstrap

Webpack

Axios

--Missing Parts

--Backend: Ef Core In Memory DB using limited. Applied work-around for identity increment db entities.

--Backend: Access Token response object is not in standard pattern.

--Backend: Refresh Token returns default token.

--Backend: User entity should be connected with repository.

--Backend: Any text/db logging should be added.(log4net, seriallog,nlog)

--FrontEnd: Applied react-bootstrap component for standardization of styles

--FrontEnd: Applied react builtin reducer and store with context api








