﻿using CargoTest.Core.Models;
using CargoTest.Workflow.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTest.Core.Workflow
{
    public class TariffManager
    {
        readonly ICargoRepository _cargoRepository;

        public TariffManager(ICargoRepository cargoRepository)
        {
            _cargoRepository = cargoRepository;
        }

        public async Task< GetTariffsResponse> GetTariffsAsync()
        {
            GetTariffsResponse getTariffsResponse = new GetTariffsResponse();
            getTariffsResponse.TariffItems = new List<TariffItem>();
            try
            {
                var tariffList = _cargoRepository.GetTariffs();
                if (tariffList == null) return getTariffsResponse;
                foreach (Tariff tariff in tariffList)
                {
                    var tariffItem = new TariffItem();
                    tariffItem.Id = tariff.Id;
                    tariffItem.CourierId = tariff.CourierId;
                    tariffItem.TariffTypeId = tariff.TariffTypeId;
                    tariffItem.IsActive = tariff.IsActive;
                    tariffItem.MinValue = tariff.MinValue;
                    tariffItem.MaxValue = tariff.MaxValue;
                    tariffItem.Fee = tariff.Fee;
                    tariffItem.OverweightPerFee = tariff.OverweightPerFee;
                    tariffItem.IsOverweightPerFee = tariff.IsOverweightPerFee;

                    getTariffsResponse.TariffItems.Add(tariffItem);
                }
            }
            catch (Exception ex)
            {
                getTariffsResponse.Errors.Add("Error while getting tariff"); //to:do parametrized
                getTariffsResponse.Description = ex.Message; //to:do parametrized
            }
            return getTariffsResponse;

        }

        public async Task<GetTariffsResponse> GetTariffsByCourierIdAsync(int courierId)
        {
            GetTariffsResponse getTariffsResponse = new GetTariffsResponse();
            getTariffsResponse.TariffItems = new List<TariffItem>();
            try
            {
                var tariffList = await _cargoRepository.GetTariffsByCourierIdAsync(courierId);
                if (tariffList == null) return getTariffsResponse;
                foreach (Tariff tariff in tariffList)
                {
                    var tariffItem = new TariffItem();
                    tariffItem.Id = tariff.Id;
                    tariffItem.CourierId = tariff.CourierId;
                    tariffItem.TariffTypeId = tariff.TariffTypeId;
                    tariffItem.IsActive = tariff.IsActive;
                    tariffItem.MinValue = tariff.MinValue;
                    tariffItem.MaxValue = tariff.MaxValue;
                    tariffItem.Fee = tariff.Fee;
                    tariffItem.OverweightPerFee = tariff.OverweightPerFee;
                    tariffItem.IsOverweightPerFee = tariff.IsOverweightPerFee;
                    getTariffsResponse.TariffItems.Add(tariffItem);
                }
            }
            catch (Exception ex)
            {
                getTariffsResponse.Errors.Add("Error while getting tariff"); //to:do parametrized
                getTariffsResponse.Description = ex.Message; //to:do parametrized
            }
            return getTariffsResponse;
        }


        public async Task< GetTariffTypesResponse> GetTariffTypesAsync()
        {
            GetTariffTypesResponse getTariffsResponse = new GetTariffTypesResponse();
            getTariffsResponse.TariffTypes = new List<TariffType>();
            try
            {
                var tariffTypeList = _cargoRepository.GetTariffTypes();
                if (tariffTypeList == null) return getTariffsResponse;
                foreach (TariffType tariffType in tariffTypeList)
                {
                    getTariffsResponse.TariffTypes.Add(tariffType);
                }
            }
            catch (Exception ex)
            {
                getTariffsResponse.Errors.Add("Error while getting tariff"); //to:do parametrized
                getTariffsResponse.Description = ex.Message; //to:do parametrized
            }
            return getTariffsResponse;

        }
    }

}
