﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTest.Workflow.Model
{
    public class GetCourierRequest
    {
        public decimal Id { get; set; }
    }

    public class GetCourierResponse : ResponseBase
    {
        public List<CourierItem> CourierItems { get; set; }
    }

    public class CourierItem
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string Category { get; set; }
        public bool IsActive { get; set; }
    }

    public class AddCourierRequest
    {
        public string? Name { get; set; }
        public string Category { get; set; }
    }

    public class AddCourierResponse : ResponseBase
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string Category { get; set; }
    }



    public class UpdateCourierResponse : ResponseBase
    {
    }

    public class UpdateCourierRequest
    {
        public CourierItem CourierItem { get; set; }
    }


    public class GetCourierByIdResponse:ResponseBase 
    {
        public CourierItem CourierItem { get; set; }
    }
    

}
