﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTest.Workflow.Model
{

    public class GetOrderResponse : ResponseBase
    {
        public List<OrderItem> OrderItems { get; set; }
    }

    public class OrderItem
    {
        public long Id { get; set; }
        public long ParcelOfferId { get; set; }
        public short Status { get; set; }
        public bool IsActive { get; set; }
    }


    public class AddOrderResponse : ResponseBase
    {
        public long Id { get;  set; }
    }

    public class AddOrderRequest
    {
        public  List<ParcelOfferItem> ParcelOfferItems { get;  set; } //will added to db for reporting

    }
}
