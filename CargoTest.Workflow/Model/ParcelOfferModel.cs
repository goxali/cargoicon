﻿using CargoTest.Core;
using CargoTest.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTest.Workflow.Model
{
    public class GetParcelOfferRequest
    {
        public decimal Width { get; set; }

        public decimal Height { get; set; }

        public decimal Depth { get; set; }

        public decimal Weight { get; set; }
    }

    public class GetParcelOfferResponse : ResponseBase
    {
        public List<ParcelOfferItem>? ParcelOfferItems { get; set; }
    }

    public class ParcelOfferItem
    {
        public CourierItem Courier { get; set; }
        public decimal Weight { get; set; }
        public decimal Volume { get; set; }
        public decimal FinalPrice { get; set; }
        public bool Selected { get; set; }
    }

    public class GetParcelOfferByUserIdRequest
    {
        public long UserId { get; set; }
    }

    public class GetParcelOfferByUserIdResponse : ResponseBase
    {
        public int CourierId { get; set; }
        public decimal Weight { get; set; }
        public decimal Volume { get; set; }
        public decimal FinalPrice { get; set; }
    }

    public class AddParcelOfferResponse : ResponseBase
    {
        public long Id { get; internal set; }
    }

    public class AddParcelOfferRequest
    {
        public int CourierId { get; internal set; }
        public decimal Volume { get; internal set; }
        public decimal Weight { get; internal set; }
        public decimal FinalPrice { get; internal set; }
    }


    public class Parcel
    {
        public Parcel(int courierId,decimal width, decimal height, decimal depth, decimal weight)
        {
            this.CourierId = courierId;
            this.Width = width;
            this.Height = height;
            this.Depth = depth;
            this.Weight = weight;
        }

        public int CourierId { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public decimal Depth { get; set; }
        public decimal Weight { get; set; }
        public decimal Volume => (decimal)(Weight * Height * Depth);
        

    }



}
