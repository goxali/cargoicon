﻿using CargoTest.Core.Models;
using CargoTest.Workflow.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTest.Core.Workflow
{
    public class CourierManager
    {

        readonly ICargoRepository _cargoRepository;
        public CourierManager(ICargoRepository cargoRepository)
        {
            _cargoRepository = cargoRepository;
        }
        public async Task<GetCourierResponse> GetCouriersAsync()
        {
            GetCourierResponse getCourierResponse = new GetCourierResponse();
            getCourierResponse.CourierItems = new List<CourierItem>();
            try
            {
                var courierList = await _cargoRepository.GetCouriers();
                if (courierList == null) return getCourierResponse;
                foreach (Courier courier in courierList)
                {
                    var courierItem = new CourierItem();
                    courierItem.Id = courier.Id;
                    courierItem.Name = courier.Name;
                    courierItem.Category = courier.Category;
                    getCourierResponse.CourierItems.Add(courierItem);
                }
            }
            catch (Exception ex)
            {
                getCourierResponse.Errors.Add("Error while getting courier"); //to:do parametrized
                getCourierResponse.Description = ex.Message; //to:do parametrized
            }
            return getCourierResponse;

        }

        public async Task<AddCourierResponse> AddCourierAsync(AddCourierRequest AddCourierRequest)
        {
            AddCourierResponse addCourierResponse = new AddCourierResponse();
            try
            {
                var courier = new Courier();
                courier.Name = AddCourierRequest.Name;
                courier.Category = AddCourierRequest.Category;
                courier.IsActive = true;
                await _cargoRepository.AddCourierAsync(courier); // Added
            }
            catch (Exception ex)
            {
                addCourierResponse.Errors.Add("Error while adding courier"); //to:do parametrized
                addCourierResponse.Description = ex.Message; //to:do parametrized
            }
            return addCourierResponse;

        }

        public async Task<UpdateCourierResponse> UpdateCourierAsync(UpdateCourierRequest updateCourierRequest)
        {
            UpdateCourierResponse updateCourierResponse = new UpdateCourierResponse();
            try
            {
                var courier = _cargoRepository.GetCourier(updateCourierRequest.CourierItem.Id);
                if (courier == null) throw new Exception("No courier found");

                courier.Name = updateCourierRequest.CourierItem.Name;
                courier.Category = updateCourierRequest.CourierItem.Category;
                courier.IsActive = updateCourierRequest.CourierItem.IsActive;

                await _cargoRepository.UpdateCourierAsync(courier);

            }
            catch (Exception ex)
            {
                updateCourierResponse.Errors.Add("Error while updating courier"); //to:do parametrized
                updateCourierResponse.Description = ex.Message; //to:do parametrized
            }
            return updateCourierResponse;
        }

        public async Task<GetCourierByIdResponse> GetCourierByIdAsync(int id)
        {
            GetCourierByIdResponse getCourierByIdResponse= new GetCourierByIdResponse();
            var response = await GetCouriersAsync();
            getCourierByIdResponse.CourierItem = response.CourierItems.FirstOrDefault(x => x.Id == id);
            return getCourierByIdResponse;
        }
    }

}
