﻿using CargoTest.Core.Models;
using CargoTest.Workflow.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTest.Core.Workflow
{
    public class OrderManager
    {
        readonly ICargoRepository _cargoRepository;

        public OrderManager(ICargoRepository cargoRepository)
        {
            _cargoRepository = cargoRepository;
        }

        public async Task<GetOrderResponse> GetOrdersAsync()
        {
            GetOrderResponse getCourierResponse = new GetOrderResponse();
            getCourierResponse.OrderItems = new List<OrderItem>();
            try
            {
                var orderList = _cargoRepository.GetOrders();
                if (orderList == null) return getCourierResponse;
                foreach (Order order in orderList)
                {
                    var orderItem = new OrderItem();
                    orderItem.Id = order.Id;
                    orderItem.ParcelOfferId = order.ParcelOfferId;
                    orderItem.Status = order.Status;
                    orderItem.IsActive = order.IsActive;
                    getCourierResponse.OrderItems.Add(orderItem);
                }
            }
            catch (Exception ex)
            {
                getCourierResponse.Errors.Add("Error while getting order"); //to:do parametrized
                getCourierResponse.Description = ex.Message; //to:do parametrized
            }
            return getCourierResponse;

        }

        public async Task<GetOrderResponse> GetOrdersByUserIdAsync(int userId)
        {
            GetOrderResponse getCourierResponse = new GetOrderResponse();
            getCourierResponse.OrderItems = new List<OrderItem>();
            try
            {
                var orderList = _cargoRepository.GetOrdersByUserId(userId);
                if (orderList == null) return getCourierResponse;
                foreach (Order order in orderList)
                {
                    var orderItem = new OrderItem();
                    orderItem.Id = order.Id;
                    orderItem.ParcelOfferId = order.ParcelOfferId;
                    orderItem.Status = order.Status;
                    orderItem.IsActive = order.IsActive;
                    getCourierResponse.OrderItems.Add(orderItem);
                }
            }
            catch (Exception ex)
            {
                getCourierResponse.Errors.Add("Error while getting order"); //to:do parametrized
                getCourierResponse.Description = ex.Message; //to:do parametrized
            }
            return getCourierResponse;

        }

        public async Task<AddOrderResponse> AddOrderAsync(AddOrderRequest addCourierRequest)
        {
            AddOrderResponse addCourierResponse = new AddOrderResponse();
            try
            {
                var order = new Order();
                var parcelOfferManager = new ParcelOfferManager(_cargoRepository);
                var selected = addCourierRequest.ParcelOfferItems.FirstOrDefault(x => x.Selected);
                if (selected == null) throw new Exception("No selected parcel offer");
                var parcelOfferId = await parcelOfferManager.AddParcelOffersAsync(addCourierRequest.ParcelOfferItems);
                if (!parcelOfferId.HasValue) throw new Exception("error adding parcel offers");
                order.ParcelOfferId = (long)parcelOfferId;
                order.Status = 1;
                order.IsActive = true;
                addCourierResponse.Id= await _cargoRepository.AddOrderAsync(order);
            }
            catch (Exception ex)
            {
                addCourierResponse.Errors = new List<string>
                {
                    "Error while adding order" //to:do parametrized
                };
                addCourierResponse.Description = ex.Message; //to:do parametrized
            }
            return addCourierResponse;

        }

    }

}
