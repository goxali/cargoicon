﻿using CargoTest.Core.Models;
using CargoTest.Workflow.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTest.Core.Workflow
{
    public class ParcelOfferManager
    {

        readonly ICargoRepository _cargoRepository;

        public ParcelOfferManager(ICargoRepository cargoRepository)
        {
            _cargoRepository = cargoRepository;
        }

        public async Task<GetParcelOfferResponse> GetParcelOffersAsync(GetParcelOfferRequest getParcelOfferRequest)
        {
            GetParcelOfferResponse parcelOfferResponse = new GetParcelOfferResponse();
            parcelOfferResponse.ParcelOfferItems = new List<ParcelOfferItem>();
            CourierManager courierManager = new CourierManager(_cargoRepository);
            var courierListResponse = await courierManager.GetCouriersAsync();
            if (courierListResponse == null) return parcelOfferResponse;
            foreach (CourierItem courier in courierListResponse.CourierItems)
            {
                var parcel =
                    new Parcel(courier.Id, getParcelOfferRequest.Width, getParcelOfferRequest.Height, getParcelOfferRequest.Depth, getParcelOfferRequest.Weight);
                var parcelOfferItem = new ParcelOfferItem();
                parcelOfferItem.Courier = courier;
                parcelOfferItem.Weight = parcel.Weight;
                parcelOfferItem.Volume = parcel.Volume;
                parcelOfferItem.FinalPrice = await GetFinalPrice(parcel);

                if (parcelOfferItem.FinalPrice > 0) parcelOfferResponse.ParcelOfferItems.Add(parcelOfferItem);
            }
            return parcelOfferResponse;
        }

        public async Task<AddParcelOfferResponse> AddParcelOfferAsync(AddParcelOfferRequest addParcelOfferRequest)
        {
            var parcelOffer = new ParcelOffer()
            { Volume = addParcelOfferRequest.Volume, Weight = addParcelOfferRequest.Weight };
            parcelOffer.Courier = _cargoRepository.GetCourier(addParcelOfferRequest.CourierId);
            parcelOffer.FinalPrice = addParcelOfferRequest.FinalPrice;
            long id = await _cargoRepository.AddParcelOfferAsync(parcelOffer);
            AddParcelOfferResponse addParcelOfferResponse = new AddParcelOfferResponse();
            addParcelOfferResponse.Id = id;
            return addParcelOfferResponse;

        }

        public async Task<GetParcelOfferByUserIdResponse> GetParcelOfferByUserIdAsync(GetParcelOfferByUserIdRequest getParcelOfferByUserIdRequest)
        {
            var getParcelOfferByUserIdResponse = new GetParcelOfferByUserIdResponse();
            ParcelOffer parcelOffer = await _cargoRepository.GetParcelOffer(getParcelOfferByUserIdRequest.UserId);
            getParcelOfferByUserIdResponse.CourierId = parcelOffer.Courier.Id;
            getParcelOfferByUserIdResponse.Volume = parcelOffer.Volume;
            getParcelOfferByUserIdResponse.Weight = parcelOffer.Weight;
            getParcelOfferByUserIdResponse.FinalPrice = parcelOffer.FinalPrice;
            return getParcelOfferByUserIdResponse;
        }

        private async Task<decimal> GetFinalPrice(Parcel parcelOffer)
        {
            decimal? feeByVolume = await GetFeeByVolume(parcelOffer);
            decimal? feeByWeight = await GetFeeByWeight(parcelOffer);
            decimal finalFeeByVolume = feeByVolume.HasValue ? (decimal)feeByVolume : 0M; //To:Do zero valeu should be validated
            decimal finalFeeByWeight = feeByWeight.HasValue ? (decimal)feeByWeight : 0M; //To:Do zero valeu should be validated
            decimal finalPrice = Math.Max(finalFeeByVolume, finalFeeByWeight);
            return finalPrice;
        }

        private async Task<decimal?> GetFeeByWeight(Parcel parcelOffer)
        {
            decimal? weightFee = null;
            var weightTariffs = await _cargoRepository.GetTariffsByCourierIdAsync(parcelOffer.CourierId);
            var weightTariff =
                weightTariffs.Where(x => x.TariffTypeId == (short)TariffType.Weight
                && x.MinValue < parcelOffer.Weight && (x.MaxValue == null || x.MaxValue >= parcelOffer.Weight))
                .FirstOrDefault();
            if (weightTariff == null) return null;
            weightFee =
                weightTariff.IsOverweightPerFee ?
                weightTariff.Fee + ((parcelOffer.Weight - weightTariff.MinValue) * weightTariff.OverweightPerFee)
                : weightTariff.Fee
                ;
            return weightFee;
        }

        private async Task<decimal?> GetFeeByVolume(Parcel parcelOffer)
        {
            decimal? volumeFee = null;
            var volumeTariffs = await _cargoRepository.GetTariffsByCourierIdAsync(parcelOffer.CourierId);
            var volumeTariff =
                volumeTariffs.Where(x => x.TariffTypeId == (short)TariffType.Volume
                && x.MinValue < parcelOffer.Volume && (x.MaxValue == null || x.MaxValue >= parcelOffer.Volume))
                .FirstOrDefault();
            if (volumeTariff == null) return null;
            volumeFee = volumeTariff.Fee;
            return volumeFee;
        }

        internal async Task<long?> AddParcelOffersAsync(List<ParcelOfferItem> parcelOfferItems)
        {
            long? selectedParcelOfferId = null;
            foreach (ParcelOfferItem parcelOfferItem in parcelOfferItems)
            {
                var parcelOffer = new ParcelOffer()
                {
                    Courier = new Courier()
                    {
                        Id = parcelOfferItem.Courier.Id,
                        Category = parcelOfferItem.Courier.Category,
                        IsActive = parcelOfferItem.Courier.IsActive,
                        Name = parcelOfferItem.Courier.Name
                    },
                    FinalPrice = parcelOfferItem.FinalPrice,
                    Volume = parcelOfferItem.Volume,
                    Weight = parcelOfferItem.Weight
                };
                long id = await _cargoRepository.AddParcelOfferAsync(parcelOffer);
                if (parcelOfferItem.Selected) selectedParcelOfferId = id;
            }
            return selectedParcelOfferId;
        }
    }
}
