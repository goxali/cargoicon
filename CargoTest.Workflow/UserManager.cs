﻿using CargoTest.Core;
using CargoTest.Core.Models;
using CargoTest.Core.Repository;
using CargoTest.Workflow.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargoTest.Workflow
{
    public class UserManager
    {

        readonly ICargoRepository _cargoRepository;
        public UserManager(ICargoRepository cargoRepository)
        {
            _cargoRepository = cargoRepository;
        }
        public async Task AuthenticateUser(UserLoginRequest userLoginRequest)
        {
            await _cargoRepository.GetUserByLogin(userLoginRequest.UserName, userLoginRequest.Password);
        }

        public async Task<UserResponse?> GetUserLoginResponse(UserLoginRequest userLoginRequest)
        {
            var userEntity = await _cargoRepository.GetUserByLogin(userLoginRequest.UserName, userLoginRequest.Password);
            if (userEntity == null) return null;
            var userResponse = MapUserToResponse(userEntity);
            return userResponse;

        }

        private UserResponse MapUserToResponse(User? userEntity)
        {
            if (userEntity == null) return null;
            return new UserResponse()
            {
                Email = userEntity.Email,
                FullName = userEntity.FullName,
                Name = userEntity.Name,
                SurName = userEntity.SurName,
                UserName = userEntity.UserName
            };

        }
    }
}
