import { useState } from 'react';
import Card from 'react-bootstrap/Card';

function Order(props) {

    const [orderId] = useState(props.orderId);
    return (
        <Card style={{ width: '18rem' }}>
            <Card.Body>
                <Card.Title>Order : {orderId}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">Status</Card.Subtitle>
                <Card.Text>
                    Explanation about order
                </Card.Text>
            </Card.Body>
        </Card>
    );
}

export default Order;