import {Button,Card} from 'react-bootstrap';

function Home() {
    const handleClick = () => {
        window.open('https://www.icon.com.mt/', '_blank');
    };
    return (
        <Card>
            <Card.Body>
                <Card.Title>This is Icon Test project</Card.Title>
                <Card.Text>Looking for digital transformation services, custom software development or wish to discuss a new project? We’d love to hear from you.</Card.Text>
                <Card.Text>
                    https://www.icon.com.mt/
                </Card.Text>
                <Button variant="primary" onClick={handleClick}>Go to Icon</Button>
            </Card.Body>
        </Card>
    );
}

export default Home;