import React, { useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { setUserSession } from '../utils/common';
import { Toast, Form, Row, Col } from 'react-bootstrap';
import { UserContext } from '../context/UserContext'
import AuthService from '../services/auth.service'


const Login = (props) => {

    const history = useNavigate();
    const username = useFormInput('');
    const password = useFormInput('');
    const { setUser, setIsLoggedIn } = useContext(UserContext);
    const [error, setError] = useState();
    const [loading, setLoading] = useState(false);
    const [show, setShow] = useState(false);

    const handleLogin = () => {
        setError();
        setLoading(true);
        AuthService.createToken(username.value, password.value).then(response => {
            setLoading(false);
            setUserSession(response.data, response.data.user);
            setUser(username)
            setIsLoggedIn(true)
            history('/');
        }).catch(err => {
            setLoading(false);
            console.log(err)
            if (err.response.status === 401) setError(err.response.data.message);
            else setError("Something went wrong. Please try again later.");
            setShow(true);
        });
    }

    return (
        <Form>
            <Toast onClose={() => setShow(false)} show={show} autohide>
                <Toast.Header>
                    <strong className="me-auto">Warning</strong>
                </Toast.Header>
                <Toast.Body>Wrong user or password.{error}</Toast.Body>
            </Toast>
            <Row >
                <Col md={3} className="mb-4">
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" {...username} autoComplete="new-password" >
                    </Form.Control>
                </Col>
            </Row>
            <Row >
                <Col md={3} className="mb-4">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" {...password} autoComplete="new-password" />
                </Col>
            </Row>
            <Row >
                <Col md={3} className="mb-4">
                    <input type="button" className="btn btn-primary"
                        value={loading ? "Loading..." : "Login"}
                        onClick={handleLogin} disabled={loading} />
                </Col>
            </Row>
        </Form>
    );
}

const useFormInput = initialValue => {
    const [value, setValue] = useState(initialValue);

    const handleChange = e => {
        setValue(e.target.value);
    }
    return {
        value,
        onChange: handleChange
    }
}

export default Login;