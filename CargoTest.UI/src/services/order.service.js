import axios from "axios";
import { getToken } from '../utils/common'


const API_URL = "https://localhost:7173/api/";
const config = {
    headers: {
        'Content-Type': 'application/json',
    }
}

const getOrders = (userId) => {
    const token = getToken();
    const axiosInstance = axios.create({
        baseURL: API_URL
    });
    axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`
    return axiosInstance.get(API_URL + "Orders/" + userId, config);
};

const OrderService = {
    getOrders,
}

export default OrderService;