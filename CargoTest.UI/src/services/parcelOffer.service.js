import axios from "axios";
import { getToken } from '../utils/common'


const API_URL = "https://localhost:7173/api/";
const config = {
    headers: {
        'Content-Type': 'application/json',
    }
}

const getParcelOffers = (params) => {
    const token = getToken();
    const axiosInstance = axios.create({
        baseURL: API_URL,
        params: params
    });
    axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`
    return axiosInstance.get(API_URL + "ParcelOffers", config);
};


const addOrder = (offers) => { 
    const token = getToken();
    const axiosInstance = axios.create({
      baseURL: API_URL
    });
    axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`
    return axiosInstance.post(API_URL + "Orders", JSON.stringify({ parcelOfferItems: offers }), config);
};

const ParcelOfferService = {
    getParcelOffers,
    addOrder
}

export default ParcelOfferService;