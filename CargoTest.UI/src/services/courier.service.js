import axios from "axios";
import { getToken } from '../utils/common'


const API_URL = "https://localhost:7173/api/";
const config = {
  headers: {
    'Content-Type': 'application/json',
  }
}

const getCouriers = () => {
  const token = getToken();
  const axiosInstance = axios.create({
    baseURL: API_URL
  });
  axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`
  return axiosInstance.get(API_URL + "Couriers", config);
};

const addCourier = (courier) => {
  const token = getToken();
  const axiosInstance = axios.create({
    baseURL: API_URL
  });
  axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`
  return axiosInstance.post(API_URL + "Couriers", JSON.stringify(courier), config);
};

const updateCourier = (id, courier) => {
  const token = getToken();
  const axiosInstance = axios.create({
    baseURL: API_URL
  });
  axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`
  return axiosInstance.put(API_URL + "Couriers/" + id, JSON.stringify({ courierItem: courier }), config);
};

const deleteCourier = (id) => {
  const token = getToken();
  const axiosInstance = axios.create({
    baseURL: API_URL
  });
  axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${token}`
  return axiosInstance.delete(API_URL + "Couriers/" + id, config);
};

const CourierService = {
  addCourier,
  updateCourier,
  deleteCourier,
  getCouriers,
}

export default CourierService;