
import React,{useContext} from 'react';
import { UserContext } from '../../App';

function User() {
    const value = useContext(UserContext); 
    return  <h1>{value}</h1>;
    
  }

  export default User;