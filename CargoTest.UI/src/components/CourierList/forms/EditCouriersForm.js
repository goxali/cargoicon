import React, { useState, useEffect } from "react";

const EditCouriersForm = (props) => {
  const [courier, setCourier] = useState(props.currentCourier);
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setCourier({ ...courier, [name]: value, isActive: true });
  };
  useEffect(() => {
    setCourier(props.currentCourier);
  }, [props]);

  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        props.updateCourier(courier.id, courier);
      }}
    >
      <label>Name</label>
      <input
        type="text"
        name="name"
        value={courier.name}
        onChange={handleInputChange}
      />
      <label>Category</label>
      <input
        type="text"
        name="category"
        value={courier.category}
        onChange={handleInputChange}
      />
      <button className="btn btn-success">Update User</button>
      <button
        onClick={() => props.setEditing(false)}
        className="btn btn-warning">
        Cancel
      </button>
    </form>
  );
};
export default EditCouriersForm;
