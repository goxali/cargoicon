import React, { useState } from "react";

const AddCourierForm = (props) => {
  const initialFormState = { id: null, name: "", category: "" };
  const [courier, setCourier] = useState(initialFormState);

  const inputHandler = (event) => {
    const { name, value } = event.target;
    setCourier({ ...courier, [name]: value });
  };

  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        if (!courier.name || !courier.category) {
          alert("Enter both a Name and Category please!");
          return;
        } else {
          props.addCourier(courier);
          setCourier(initialFormState);
        }
      }}
    >
      <label>Name</label>
      <input
        type="text"
        name="name"
        placeholder="Enter a name..."
        value={courier.name}
        onChange={inputHandler}
      />
      <label>Category</label>
      <input
        type="text"
        name="category"
        placeholder="Enter a category..."
        value={courier.category}
        onChange={inputHandler}
      />
      <button className="btn btn-success">Add New Courier</button>
    </form>
  );
};

export default AddCourierForm;
