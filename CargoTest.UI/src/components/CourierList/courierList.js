import React, { useState, useEffect, Fragment } from "react";
import CourierTable from "./tables/CourierTable";
import AddCourierForm from "./forms/AddCourierForm";
import EditCouriersForm from "./forms/EditCouriersForm";
import './index.css';
import { Container } from 'react-bootstrap';
import CourierService from '../../services/courier.service'

const CourierList = () => {

  const initialFormState = { id: null, name: "", category: "" };
  const [couriers, setCouriers] = useState([]);
  const [editing, setEditing] = useState(false);
  const [currentCourier, setCurrentCourier] = useState(initialFormState);
  const [refreshList, setRefreshList] = useState(0);

  useEffect(() => {
    CourierService.getCouriers().then(response => {
      setCouriers(response.data.courierItems);
    }).catch(err => { console.error(err.message); });
  }, [refreshList]);

  const addCourier = async (courier) => {
    await CourierService.addCourier(courier).then(() => {
    }).catch(err => { console.error(err.message); });
    setRefreshList(oldKey => oldKey + 1)
  };

  const deleteCourier = async (courier) => {
    setEditing(false);
    await CourierService.deleteCourier(courier).then(() => {
    }).catch(err => { console.error(err.message); });
    setRefreshList(oldKey => oldKey + 1)
  };

  const editRow = (courier) => {
    setEditing(true);
    setCurrentCourier({ id: courier.id, name: courier.name, category: courier.category });
  };

  const updateCourier = async (id, updatedCourier) => {
    setEditing(false);
    await CourierService.updateCourier(id, updatedCourier).then(() => {
    }).catch(err => { console.error(err.message); });
    setRefreshList(oldKey => oldKey + 1)
    //setCouriers(couriers.map((courier) => (courier.id === id ? updatedCourier : courier)));
  };

  return (

    <Container>
      <div className="flex-row">
        <div className="flex-large">
          {editing ? (
            <Fragment>
              <h2>Edit Courier</h2>
              <EditCouriersForm
                editing={editing}
                setEditing={setEditing}
                currentCourier={currentCourier}
                updateCourier={updateCourier}
                deleteCourier={deleteCourier}
              />
            </Fragment>
          ) : (
            <Fragment>
              <h2>Add Courier</h2>
              <AddCourierForm addCourier={addCourier} />
            </Fragment>
          )}
        </div>
        <div className="flex-large">
          <h2>View Couriers</h2>
          <CourierTable couriers={couriers} editRow={editRow} deleteCourier={deleteCourier} />
        </div>
      </div>
    </Container>
  );
};

export default CourierList;
