import React from "react";

const CourierTable = (props) => (
  <table>
    <thead>
      <tr>
        <th>Name</th>
        <th>Category</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {props.couriers.length > 0 ? (
        props.couriers.map((courier) => (
          <tr key={courier.id}>
            <td>{courier.name}</td>
            <td>{courier.category}</td>
            <td>
              <button disabled={courier.category==="Internal"}
                onClick={() => {
                  props.editRow(courier);
                }}
                className="btn btn-warning" >
                Edit
              </button>
              <button disabled={courier.category==="Internal"}
                onClick={() => props.deleteCourier(courier.id)}
                className="btn btn-danger" >
                Delete
              </button>
            </td>
          </tr>
        ))
      ) : (
        <tr>
          <td colSpan={3}>No Couriers....</td>
        </tr>
      )}
    </tbody>
  </table>
);

export default CourierTable;
