import React, { useState, Fragment } from "react";
import ParcelOfferTable from "./ParcelOfferTable";
import FilterParcelOfferForm from "./FilterParcelOfferForm";
import OrderSuccesful from "./OrderSuccesful";
import './index.css'
import ParcelOfferService from "../../services/parcelOffer.service";
import OrderService from "../../services/order.service";
import { Container } from 'react-bootstrap';
import _ from "lodash";


const ParcelOfferList = () => {

    const [offers, setOffers] = useState([]);
    const [orderId, setOrderId] = useState();
    const [order, setOrder] = useState(null);
    const [currentOffer, setCurrentOffer] = useState();

    const listOffers = async (filterForm) => {
        const params = new URLSearchParams({
            width: filterForm.width,
            height: filterForm.height,
            depth: filterForm.depth,
            weight: filterForm.weight,
        });
        await ParcelOfferService.getParcelOffers(params)
            .then((response) => { setOffers(response.data.parcelOfferItems); })
            .catch(err => { console.error(err.message); });
    };

    const orderOffer = async (offer) => {
        setCurrentOffer(offer);
        console.log(JSON.stringify(offer));
        console.log(JSON.stringify(offers));
        setOffers(offers.map((offer) => (_.isEqual(JSON.stringify(offer), JSON.stringify(currentOffer)) ? { ...offer, selected: true } : offer)));

        console.log(offers);

        await ParcelOfferService.addOrder(offers)
            .then((response) => { setOrderId(response.data.orderId); });

        const userId = 1;//todo: 
        await OrderService.getOrders(userId)
            .then((response) => {
                console.log(response.data);
                setOrder(response.data)
                setOffers([]);
            });
    };


    return (
        <Container>
            {order == null ? (
                <div className="flex-column">
                    <div className="flex-start" >
                        {
                            <Fragment>
                                <FilterParcelOfferForm listOffers={listOffers} currentOffer={currentOffer} />
                            </Fragment>
                        }
                    </div>

                    <div className="flex-large">
                        <h2>View Offers</h2>
                        <ParcelOfferTable offers={offers} orderOffer={orderOffer} />
                    </div>
                </div>) : (
                <div className="flex-large">
                    <OrderSuccesful orderId={orderId} setOrder={setOrder} />
                </div>)}
        </Container>
    );
};

export default ParcelOfferList;
