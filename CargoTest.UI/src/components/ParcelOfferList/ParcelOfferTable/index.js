import React from "react";
import { Container } from 'react-bootstrap';

const ParcelOfferTable = (props) => {

  return (
    <Container>
      <table>
        <thead>
          <tr>
            <th>Courier Name</th>
            <th>FinalPrice</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {props.offers.length > 0 ? (
            props.offers.map((offer, index) => (
              <tr key={index}>
                <td>{offer.courier.name}</td>
                <td>{offer.finalPrice}</td>
                <td>
                  <button
                    onClick={() => {
                      props.orderOffer(offer); 
                    }}
                    className="btn btn-primary" >
                    Order
                  </button>
                </td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan={3}>No Offers...</td>
            </tr>
          )}
        </tbody>
      </table>
    </Container>
  );
};

export default ParcelOfferTable;
