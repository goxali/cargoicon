import React, { useState } from "react";
import { Button, Form, Row, Col } from 'react-bootstrap';

const FilterParcelOfferForm = (props) => {
  const initialFormState = { filterFormState: { width: "", height: "", depth: "", weight: "" } };
  const [filterForm, setFilterForm] = useState(initialFormState);

  const inputHandler = (event) => {
    const { name, value } = event.target;
    setFilterForm({ ...filterForm, [name]: value });
  };

  const handleSubmit = (event) => {
    // default event for form is submit (preventing with a check)
    event.preventDefault();
    if (!filterForm.width || !filterForm.height || !filterForm.depth || !filterForm.weight) {
      alert("Enter all input please!");
      return;
    } else {
      props.listOffers(filterForm);
    }
  };

  return (
    <>
      <Form
        onSubmit={handleSubmit}
      >
        <Row >
          <Col md={2} className="mb-4">
            <Form.Label>Width</Form.Label>
            <Form.Control
              required
              type="number"
              name="width"
              placeholder="Enter a width..."
              value={filterForm.width}
              onChange={inputHandler}
              min="0"
            />
          </Col>
          <Col md={2} className="mb-4">
            <Form.Label>Height</Form.Label>
            <Form.Control
              required
              type="number"
              name="height"
              placeholder="Enter a height..."
              value={filterForm.height}
              onChange={inputHandler}
              min="0"
            />
          </Col>
          <Col md={2} className="mb-4">
            <Form.Label>Depth</Form.Label>
            <Form.Control
              required
              type="number"
              name="depth"
              placeholder="Enter a depth..."
              value={filterForm.depth}
              onChange={inputHandler}
              min="0"
            />
          </Col>
          <Col md={2} className="mb-4">
            <Form.Label>Weight</Form.Label>
            <Form.Control
              required
              type="number"
              name="weight"
              placeholder="Enter a weight..."
              value={filterForm.weight}
              onChange={inputHandler}
              min="0"
            />
          </Col>
        </Row>
        <Row>
          <Col md={2} className="mb-2">
            <Button type="submit" className="btn btn-primary">List Offers</Button>
          </Col>
        </Row>
      </Form>
    </>
  );
};

export default FilterParcelOfferForm;
