import React from "react";
import { useNavigate } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

const  OrderSuccessful= (props)=> {
    const navigate = useNavigate();
    const handleOrdersClick = () => {
        navigate('/order');
    };
    const handleBackClick = () => {
       props.setOrder(null);
    };
    return (
        <Card>
            <Card.Body>
                <Card.Title>Order successful..</Card.Title>
                <Button variant="primary" onClick={handleOrdersClick}>Go to Orders</Button>
                <Button variant="primary" onClick={handleBackClick}>Back</Button>
            </Card.Body>
        </Card>
    );
}

export default OrderSuccessful;