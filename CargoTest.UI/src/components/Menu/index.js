import React, { useContext } from 'react';
import Container from 'react-bootstrap/Container';
import { Link ,useNavigate} from 'react-router-dom';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { removeUserSession } from '../../utils/common';
import { UserContext } from '../../context/UserContext';

const Menu = (props) => {
  const { isLoggedIn ,setUser, setIsLoggedIn} = useContext(UserContext);
  const history= useNavigate();
  const handleLogoutClick = ()=>
  {
    removeUserSession();
    setUser(null);
    setIsLoggedIn(false);
    history('/'); 
  }
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" className="p-3">
      <Container>
        <Navbar.Brand to="#">Icon Test</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto text-white">
            <Link className="text-decoration-none text-white ps-3" to="/">
              Home
            </Link>
            {isLoggedIn && (
              <Link className="text-decoration-none text-white ps-3" to="/courier" >
                Courier
              </Link>)}
            {isLoggedIn && (<Link className="text-decoration-none text-white ps-3" to="/parcelOffer">
              Offer
            </Link>)}
            {isLoggedIn && (<Link className="text-decoration-none text-white ps-3" to="/order">
              Orders
            </Link>)}
          </Nav>
          <Nav className="gap-2">
            {(!isLoggedIn) && (<Link className="btn btn-primary" to="/login">
              Login
            </Link>)}
            {isLoggedIn && (<Link className="btn btn-light text-black" onClick={()=>handleLogoutClick()} >
              Logout
            </Link>)}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Menu;