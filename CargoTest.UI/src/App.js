import React, { useState, useEffect  } from 'react';
import { getToken, removeUserSession, setUserSession } from './utils/common';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import Menu from './components/Menu';
import Home from './pages/Home';
import NotFound from './pages/NotFound';
import Courier from './pages/Courier';
import ParcelOffer from './pages/ParcelOffer';
import Order from './pages/Order';
import Login from './pages/Login';
import PrivateRoutes from './utils/PrivateRoutes';
import PublicRoutes from './utils/PublicRoutes';
import { UserProvider } from './context/UserContext'
import AuthService from './services/auth.service'

const App = () => {
  const [authLoading, setAuthLoading] = useState(true);

  useEffect(() => {
    const token = getToken();
    if (!token) {
      return;
    }
    AuthService.verifyToken(token).then(response => {
      setUserSession(response.data.token, response.data.user); //to:do user missing
      setAuthLoading(false);
    }).catch(error => {
      removeUserSession();
      setAuthLoading(false);
    });
  }, []);

  if (authLoading && getToken()) {
    return <div className="content">Checking Authentication...</div>
  }

  return (
    <UserProvider>
      <BrowserRouter>
        <Menu />
        <Routes>
          <Route path="*" element={<NotFound />} />

          <Route element={<PublicRoutes />}>
            <Route path="/" index element={<Home />} />
            <Route path="/login" element={<Login />} />
          </Route>

          <Route element={<PrivateRoutes />}>
            <Route path="/courier" element={<Courier />} />
            <Route path="/parcelOffer" element={<ParcelOffer />} />
            <Route path="/order" element={<Order />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
